package re.client.strategicMap

import re.server.game.basicStructures.{Point, State}
import re.server.game.units.{Archer, Fortress, Swordsman}

import scala.Console.{BLUE, MAGENTA, RED, YELLOW}

object StrategicMap {
  def renderState(state: State, actPlayerId: Long): Unit = {

    def getColourScheme(state: State): Map[Long, String] = {
      val colourLst = List(BLUE, MAGENTA, YELLOW, RED)
      val playerSorted = state.state.map(_.id).sorted
      (playerSorted zip colourLst).toMap
    }

    def isUnit(target: Point, state: State) = {
      state.state.exists(_.warfareUnits.map(_.position).contains(target)) ||
        state.state.exists(_.fortress.position == target)
    }

    def drawUnit(state: State, target: Point, colourScheme: Map[Long, String]): Unit = {
      val tupleUnitColour = {for (player <- state.state;
           warfareUnit <- player.warfareUnits
           if warfareUnit.position == target) yield (warfareUnit, colourScheme(player.id))}.headOption match {
        case Some(unit) => unit
        case None =>
          {for (player <- state.state if player.fortress.position == target)
            yield (player.fortress, colourScheme(player.id))}.head
      }
      val isBorder = if (target.x==10) 1 else 0 // переход на новую строчку границы
      tupleUnitColour match {
        case (s : Swordsman, colour) => print(colour + s"S${s.hp} " + "\n " * isBorder + Console.RESET)
        case (a : Archer, colour) => print(colour + s"A${a.hp} " + "\n " * isBorder + Console.RESET)
        case (f : Fortress, colour) =>
          if (f.hp < 10) print(colour + s"F${f.hp} " + "\n " * isBorder + Console.RESET)
          else print(colour + s"F${f.hp}" + "\n " * isBorder + Console.RESET)
      }
    }
    val actingPlayer = state.state.filter(_.id == actPlayerId).head
    val colourScheme = getColourScheme(state)
    val currentPlayerColour = colourScheme(actPlayerId)
      print(s"Turn: ${state.turnNum}\n")
      print("Commander" + currentPlayerColour +s" ${actingPlayer.name}" + Console.RESET + "\n")
      println(s"Gold: ${actingPlayer.gold}\n")
    for (y <- 1 to 11;
         x <- 0 to 10) { (x,y) match {
      case (0,1) =>   print(" 1  ")
      case (0, y) if y==10 || y==11 => print(s"$y ")
      case (0, y) => print(s"$y  ")
      case (x, y) if isUnit(Point(x,y), state) => drawUnit(state, Point(x,y), colourScheme)
      case (10, y) if y != 11 => print("*\n ")
      case (x, 11) => print(s"$x  ")
      case _ => print("*  ")
    }}
  }


}