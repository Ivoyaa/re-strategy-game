package re.client

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import re.client.client.Player

import java.net.InetSocketAddress

object StartClient1 extends App {

  val actorSystem = ActorSystem("client1")

  private val config = ConfigFactory.load()
  private val address = config.getString("client.address")
  private val port = config.getInt("client.port")

  val fullAddress = new InetSocketAddress(address, port)

  val actorClientTCP1 = actorSystem.actorOf(Player.props(fullAddress))
}

