package re.client.client

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.io.{IO, Tcp}
import akka.util.ByteString
import re.client.client.ClientSessionHandler

import java.net.InetSocketAddress

class ClientTCPHandler(remote: InetSocketAddress, listener: ActorRef) extends Actor  with ActorLogging {

  import Tcp._
  import context.system

  IO(Tcp) ! Connect(remote)

  def receive: Receive = {
    case CommandFailed(_: Connect) =>
      listener ! "CLIENT tcp: connection failed"
      context.stop(self)

    case c @ Connected(remote, local) =>
      listener ! c
      val connection = sender()
      connection ! Register(self)
      context.become {
        case data: ByteString =>
          connection ! Write(data)
        case CommandFailed(w: Write) =>
          // O/S buffer was full
          listener ! "CLIENT tcp: write failed"
        case Received(data) =>
          listener ! data
        case "close" =>
          connection ! Close

        case _: ConnectionClosed =>
          listener ! "CLIENT tcp: connection closed "
          context.stop(self)
      }
  }
}

object ClientTCPHandler {
  def props(remote: InetSocketAddress, listener: ActorRef): Props =
    Props(new ClientTCPHandler(remote, listener))
}

