package re.client.client

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.io.Tcp.Connected
import akka.util.ByteString
import io.circe.parser._
import io.circe.syntax._
import re.client.client.ClientSessionHandler.{ConnectionEstablished, EnterGame}
import re.client.client.Player.SuccessfullyJoined
import re.server.command._
import re.server.game.basicStructures.{State, TurnToHandleJson, YourTurn}

import java.net.InetSocketAddress

class ClientSessionHandler(address: InetSocketAddress, session: ActorRef) extends Actor with ActorLogging {

  val actorClientTCP = context.actorOf(ClientTCPHandler.props(address, self), "clientTCP1")


  def receive: Receive = {
    case c @ Connected(remote, local) =>
      log.info("CLIENT session: connection established {} {}",remote, local)
      session ! ConnectionEstablished

    case data: ByteString =>
      decode[YourTurn](data.utf8String) match {
        case Right(myTurn) =>
          if (myTurn.currState.turnNum == 0) {
            session ! SuccessfullyJoined(myTurn.playerId,myTurn.extraInf)
          }

          else {
            //log.info("CLIENT session: received message: {}", myTurn)
            session ! myTurn
          }
        case Left(_) =>
      }

    case EnterGame(nickname) =>
      val msg = TurnToHandleJson(0,JoinGame,State.empty,List.empty,List.empty,List.empty,List.empty, nickname).asJson
      actorClientTCP ! ByteString(msg.toString())

    case task:TurnToHandleJson =>
      actorClientTCP ! ByteString(task.asJson.toString())

    case message: String =>
      log.info("CLIENT session: received internal message: {}", message)
    case unknown =>
      log.info("CLIENT session: received unknown message: {}", unknown)
  }
}

object ClientSessionHandler {
  def props(address: InetSocketAddress, session: ActorRef) =
    Props(classOf[ClientSessionHandler], address, session)

  case object ConnectionEstablished
  case class EnterGame(nickname: String)
}
