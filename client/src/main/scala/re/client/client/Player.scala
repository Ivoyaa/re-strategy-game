package re.client.client

import akka.actor.{Actor, ActorLogging, Props}
import re.client.client.ClientSessionHandler.{ConnectionEstablished, EnterGame}
import re.client.client.Player.SuccessfullyJoined
import re.client.strategicMap.StrategicMap
import re.server.command.{EndTurn, ExitRoom, FallbackToTurn, NoSpecialCommand}
import re.server.game.basicStructures.{InGame, Lost, Point, TurnToHandleJson, Won, YourTurn}
import re.server.game.units.{Archer, Swordsman}

import java.net.InetSocketAddress
import scala.util.Try

class Player(address: InetSocketAddress) extends Actor with ActorLogging  {

  val clientSession = context.actorOf(ClientSessionHandler.props(address, self), "clientSession")

  def analyzeSituation(turn: YourTurn): Unit = {
    val actingPlayer = turn.currState.state.filter(_.id == turn.playerId).head
    actingPlayer.status match {
      case Won =>
        println("Congratulations, Commander! Enemies are defeated.")
        context.stop(self)
      case Lost =>
        println("We lost, Commander! But we will have our revenge!")
        context.stop(self)
      case InGame =>
        handleTurn(turn)
    }
  }

  def receive: Receive = {
    case turn: YourTurn =>
      analyzeSituation(turn)

    case ConnectionEstablished =>
      handleAuth()

    case SuccessfullyJoined(id, nickname) =>
      println(s"$nickname (id: $id) successfully joined! Waiting for an opponent...")


    case _ =>
      }


  def handleAuth(): Unit = {
    println("Please, enter your nickname:")
    val nickname = scala.io.StdIn.readLine()
    clientSession ! EnterGame(nickname)
  }

  def handleTurn(turn: YourTurn): Unit = {

    def getListOfActions(movesRaw: String) = Try{
        val moves = movesRaw.split(" ").map(pair => pair.split("->"))
        val res = {
          for (move <- moves)
            yield {
              val unitPosRaw = move(0).substring( 1, move(0).length() - 1 ).split(",")
              val targetPosRaw = move(1).substring( 1, move(1).length() - 1 ).split(",")
              val unitPos = Point(unitPosRaw(0).toInt, unitPosRaw(1).toInt)
              val targetPos = Point(targetPosRaw(0).toInt, targetPosRaw(1).toInt)
              (unitPos, targetPos)
            }
        }.toList
        res
      } getOrElse(List.empty)

    def getUnitsToSpawn(unitsRaw: String) = Try{
      val units = unitsRaw.split(" ")
      val res = {
        for (unit <- units)
          yield unit match {
            case "S" => Swordsman.unitCode
            case "A" => Archer.unitCode
          }
      }.toList
      res
    } getOrElse(List.empty)



    def getCommand(commandRaw: String) = commandRaw match {
      case "exit" => ExitRoom
      case comm if commandRaw.contains("fall back to turn") => Try{
        val turnNum = commandRaw.split(" ").last
        if (Try(turnNum.toInt).isSuccess) FallbackToTurn else NoSpecialCommand
      }.getOrElse(NoSpecialCommand)
      case _ => NoSpecialCommand
    }

    StrategicMap.renderState(turn.currState, turn.playerId)
    println("\nCommander! Your orders?")
    println("Moves [\033[3mformat: (2,2)->(3,1) (4,4)->(5,4)\033[0m]: ")
    val movesRaw = scala.io.StdIn.readLine() // (2,2)->(3,1) (4,4)->(5,4)
    val moves = getListOfActions(movesRaw)


    println("Spawn [\033[3mformat: A A S\033[0m]: ")
    val spawnRaw = scala.io.StdIn.readLine() // (2,2)->(3,1) (4,4)->(5,4)
    val spawn = getUnitsToSpawn(spawnRaw)


    println("Attacks on units: ")
    val attacksRaw = scala.io.StdIn.readLine() // (2,2)->(3,1) (4,4)->(5,4)
    val attacks = getListOfActions(attacksRaw)

    println("Attacks on fortresses: ")
    val attacksFortressRaw = scala.io.StdIn.readLine() // (2,2)->(3,1) (4,4)->(5,4)
    val attacksFortress = getListOfActions(attacksFortressRaw)

    println("Other commands? (\033[3mexit, fall back to turn x\033[0m)")

    var extraInf = ""
    val commandRaw = scala.io.StdIn.readLine()
    val commandTmp = getCommand(commandRaw)
    val command = commandTmp match {
      case NoSpecialCommand => EndTurn
      case FallbackToTurn =>
        extraInf += commandRaw.split(" ").last
        FallbackToTurn
      case comm => comm
    }

    clientSession ! TurnToHandleJson(
      turn.playerId
      ,command
      ,turn.currState
      ,attacks
      ,attacksFortress
      ,moves
      ,spawn
      ,extraInf
    )
    if (command == ExitRoom) {
      println("Goodbye, Commander!")
      context.stop(self)
    }
    else
      println("Awaiting enemies' actions...")

  }
}

object Player {
  def props(address: InetSocketAddress): Props =
    Props(classOf[Player], address)

  case class SuccessfullyJoined(id: Long, nickname: String)

}

