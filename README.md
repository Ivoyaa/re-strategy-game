# RE-strategy-game
[Description](https://docs.google.com/document/d/1cbXCpY_-E7xjlAPb4e7OkpAmw-c1TmdqM8StBT8HHls/edit?usp=sharing) (Russian) 

This is a game server for turn-based strategy game developed as a course work for Tinkoff Scala School. 

## Game
This is 2-4 players turn-based strategy game. Every player has their own fortress, initial amount of gold and a desire to defeat other players! 
Every turn each player is able to:
- move warfare units
- spawn warfare units through spending gold
- attack other warfare units and fortresses
- get back to one of previous turns 

The final objective is to destroy all fortresses except the controlled one. 

Currently there are three types of units: 
- Swordsman (3 hp, 2 dmg, 2 cells move distance, 1 cell attack raduis, 30 gold)
- Archer (1 hp, 1 dmg, 1 cells move distance, 3 cell attack raduis, 40 gold)
- Fortress (20 hp, priceless)

## Technologies
- Akka Classic - Server and Client implemented through classic actors model
- MongoDB (Mongo Scala driver) - used for storing and extracting game states
- Circe - used for serializing-desializing messages between Server and Client

Clients connect to Server through TCP connection. Messages are in the form of JSON string. 
[JSON example](https://docs.google.com/document/d/19Udu6exiZfuflkhs-em7_cvz_3ft1b9ydf6RteDdYZw/edit?usp=sharing).

## Client
There is a client also written in scala using Akka Classic for demonstration purposes. It provides simple console graphics and console-keyboard input. 

As the game starts client establishes connection to Server. Then asks to input name. After fulfilling game room Server sends current game state and asks for inputing
- move commands (ex. (1,1)->(2,2) (2,3)->(2,4) )
- spawn commands (ex. A A A S S where A - Archer, S - Swordsman)
- attack unit commands (same form as for move commands)
- attack fortresses commands (same form as for move commands)
- special command (ex. exit, fall back to turn 3) - exites game or starting game from required turn (currently without asking other players)


## Start Up
Locally: 
1. provide a MongoDB instance by changing url and db strings to appropriate ones  [application config](https://gitlab.com/Ivoyaa/re-strategy-game/-/blob/master/server/src/main/resources/application.conf)
2. start [Server](https://gitlab.com/Ivoyaa/re-strategy-game/-/blob/master/server/src/main/scala/re/server/StartServer.scala)
3. start [Client1](https://gitlab.com/Ivoyaa/re-strategy-game/-/blob/master/client/src/main/scala/re/client/StartClient1.scala) and [Client2](https://gitlab.com/Ivoyaa/re-strategy-game/-/blob/master/client/src/main/scala/re/client/StartClient2.scala) [optionally]


Docker:
1. provide a MongoDB instance by changing url and db strings to appropriate ones  [application config](https://gitlab.com/Ivoyaa/re-strategy-game/-/blob/master/server/src/main/resources/application.conf)
2. `sbt docker:publishLocal`
3. run docker container from the resulting image
4. change addresses and ports for the one at which docker container is hosted in [Client1](https://gitlab.com/Ivoyaa/re-strategy-game/-/blob/master/client/src/main/scala/re/client/StartClient1.scala) and [Client2](https://gitlab.com/Ivoyaa/re-strategy-game/-/blob/master/client/src/main/scala/re/client/StartClient2.scala) [optionally]
5. start [Client1](https://gitlab.com/Ivoyaa/re-strategy-game/-/blob/master/client/src/main/scala/re/client/StartClient1.scala) and [Client2](https://gitlab.com/Ivoyaa/re-strategy-game/-/blob/master/client/src/main/scala/re/client/StartClient2.scala) [optionally]










