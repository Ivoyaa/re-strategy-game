
name := "final-project-server"

version := "1.0.1"

scalaVersion := "2.13.5"

mainClass in Compile := Some("re.server.StartServer")

val AkkaVersion = "2.6.14"
val circeVersion = "0.12.3"

lazy val server = project
  .enablePlugins(UniversalPlugin)
  .enablePlugins(AshScriptPlugin)
  .enablePlugins(DockerPlugin)
  .settings(
    packageName in Docker := "re-strategy-game",
    version in Docker := "1.0.1",
    dockerExposedPorts := Seq(8888),
    dockerUsername := Some("ivoya"),
    dockerUpdateLatest := true,
    normalizedName := "re-strategy-game-server",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
      "com.typesafe.akka" %% "akka-actor-testkit-typed" % AkkaVersion % Test,
      "org.slf4j" % "slf4j-api" % "1.7.30",
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "org.mongodb.scala" %% "mongo-scala-driver" % "2.8.0",
      "io.circe" %% "circe-core" % circeVersion,
      "io.circe" %% "circe-generic" % circeVersion,
      "io.circe" %% "circe-parser" % circeVersion,
      "io.circe" %% "circe-generic-extras" % "0.13.1-M4",
      "io.netty" % "netty" % "3.10.6.Final",
      "org.scalatest" %% "scalatest" % "3.1.4" % Test
    )
  )


lazy val client = project
  .settings(
    libraryDependencies ++= Seq(
      "org.slf4j" % "slf4j-simple" % "1.7.+"
    )
  )
  .dependsOn(server)

lazy val root = project.in(file("."))
  .aggregate(server, client)