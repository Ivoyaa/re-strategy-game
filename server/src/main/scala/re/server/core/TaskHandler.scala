package re.server.core

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSelection}
import re.server.command.JoinGame
import re.server.game.EngineService
import re.server.command._
import re.server.game.basicStructures.TurnToHandleJson

import scala.util.Try

class TaskHandler extends Actor with ActorLogging {
  import TaskHandler._


 // val authService: ActorSelection = context.actorSelection("akka://server/user/auth")
  val gameService: ActorSelection = context.actorSelection("akka://server/user/game")
  var id = 0

  override def preStart() {
    log.info("SERVER task handler: starting task service")
  }

  override def receive: Receive = {

    case task: CommandTask => handlePacket(task)

    case _ => log.info("SERVER task handler: unknown message")
  }

  override def postStop() {
    log.info("SERVER task handler: stopping task service")
  }


  def handlePacket(task: CommandTask): Unit = {
    task.turnToHandle.command match {
      case JoinGame =>
        id += 1    // Задает id каждого игрока
        gameService ! EngineService.JoinGameReq(task.session
          ,id
          ,task.turnToHandle.extraInfo
          ,task.turnToHandle.currState
        )
        log.info("SERVER task handler: request for joining: {}, {}", id, task.turnToHandle.extraInfo)

      case ExitRoom =>
        gameService ! EngineService.ExitGameReq(task.session, task.turnToHandle)

      case EndTurn =>
        gameService ! EngineService.Turn(task.session, task.turnToHandle)

      case FallbackToTurn =>
        // на клиентской стороне проводится проверка на адекватность
        // но если все же что-то пошло не так, обрабатываем как обычный ход
        val turnNumStr = task.turnToHandle.extraInfo
        if (Try(turnNumStr.toInt).isSuccess)
          gameService ! EngineService.FallBackToReq(task.session, turnNumStr.toInt, task.turnToHandle.currState)
        else
          handlePacket(task.copy(turnToHandle = task.turnToHandle.copy(command = EndTurn)))

      case _ => // log.info("SERVER task handler: unknown message")
    }
  }

}

object TaskHandler {
  final case class CommandTask(session: ActorRef, turnToHandle: TurnToHandleJson)
}