package re.server.core.db

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.typesafe.config.ConfigFactory
import io.circe.parser.decode
import io.circe.syntax.EncoderOps
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.model.Projections.excludeId
import org.mongodb.scala.{Completed, Document, MongoClient, MongoCollection, MongoDatabase}
import re.server.game.basicStructures.State

import java.util.UUID
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Success

class StorageHandler(engineRef: ActorRef, uuid: UUID) extends Actor with ActorLogging {
  import StorageHandler._

  private val config = ConfigFactory.load()
  private val mongoURL = config.getString("mongo.url")
  private val mongoDBName = config.getString("mongo.db")

  private val mongoClient: MongoClient = MongoClient(mongoURL)
  private val database: MongoDatabase = mongoClient.getDatabase(mongoDBName)
  private val collection: MongoCollection[Document] = database.getCollection(s"session $uuid")


  override def preStart() {
    log.info("SERVER storage service: starting storage service")
  }

  override def receive: Receive = {
    case task: DBSave => handleSaving(task)

    case task: DBRetrieveState => handleRetrieving(sender, task)

    case _ => log.info("SERVER storage service: unknown message")
  }

  override def postStop() {
    log.info("SERVER storage service: stopping storage service")
  }

  def handleSaving(task: DBSave): Future[Completed] = {
    val doc = Document(task.state.asJson.toString())
    collection.insertOne(doc).head()
  }

  def handleRetrieving(sender: ActorRef, task: DBRetrieveState): Unit = {
    collection
      .find(equal("turnNum", task.turnNum))
      .projection(excludeId())
      .head()
      .onComplete{
        case Success(state) =>
          sender ! decode[State](state.toJson()).getOrElse(State.empty)
      }
  }
}


object StorageHandler {
  def props(engineRef: ActorRef, uuid: UUID): Props =
    Props(new StorageHandler(engineRef, uuid))


  // ----- API -----
  case class DBSave(state: State)
  case class DBRetrieveState(turnNum: Int)
}
