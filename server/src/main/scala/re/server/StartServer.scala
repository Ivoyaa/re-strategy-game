package re.server

import akka.actor.{ActorRef, ActorSystem, Props}
import com.typesafe.config.ConfigFactory
import re.server.core.TaskHandler
import re.server.core.db.StorageHandler
import re.server.front.TCPHandler
import re.server.game.Engine

import java.util.UUID

object StartServer extends App {

  private val actorSystem = ActorSystem("server")
  private val maxPlayers = 2

  def storageMaker: (ActorRef, UUID) => Props = (actorRef, uuid) => StorageHandler.props(actorRef, uuid)

  private val config = ConfigFactory.load()
  private val address = config.getString("server.address")
  private val port = config.getInt("server.port")

  val actorTasks = actorSystem.actorOf(Props[TaskHandler], "task")
  val actorGame = actorSystem.actorOf(Engine.props(storageMaker, maxPlayers), "game")
  val actorNet = actorSystem.actorOf(TCPHandler.props(address, port), "front")

}
