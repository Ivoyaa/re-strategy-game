package re.server.front

import akka.actor.{Actor, ActorLogging, Props}
import akka.io.Tcp._
import akka.io._

import java.net.InetSocketAddress




class TCPHandler(address: String, port: Int) extends Actor with ActorLogging {
  var idCounter = 0L

  override def preStart() {
    log.info("SERVER tcp: starting TCP net server")

    import context.system
    val opts = List(SO.KeepAlive(on = true), SO.TcpNoDelay(on = true))
    IO(Tcp) ! Bind(self, new InetSocketAddress(address, port), options = opts)
  }


  def receive: Receive = {

    case b @ Bound(localAddress) =>
      log.info("SERVER tcp: tcp server started listening {}", localAddress)

    case CommandFailed(_: Bind) =>
      log.info("SERVER tcp: command failed tcp server")
      context.stop(self)

    case c @ Connected(remote, local) =>
      log.info("SERVER tcp: new incoming tcp connection on server")
      createSession(remote, local)

  }


  def createSession(remote: InetSocketAddress, local: InetSocketAddress): Unit = {
    idCounter += 1

    val session = context.actorOf(SessionHandler.props(idCounter, sender()))

    sender() ! Register(session)
  }
}

object TCPHandler {

  def props(address: String, port: Int): Props =  Props(new TCPHandler(address, port))
}