package re.server.front

import akka.actor._
import akka.io.Tcp
import akka.io.Tcp.{Close, Received, Write}
import akka.util.ByteString

import scala.concurrent.duration._
import re.server.core.TaskHandler._
import io.circe.parser._
import io.circe.syntax._
import re.server.game.basicStructures.{State, TurnToHandleJson, YourTurn}

class SessionHandler(
               val id: Long,
               connection: ActorRef,
             ) extends Actor with ActorLogging {

  import context._
  import SessionHandler._

  val taskService: ActorSelection = context.actorSelection("akka://server/user/task")
  var buffer = YourTurn(0,State.empty,"")

  // ----- heartbeat -----
  private var scheduler: Cancellable = _

  override def preStart() {
    scheduler = context.system.scheduler.schedule(period, period, self, Heartbeat)

    log.info("Session start: {}", toString)
  }

  override def receive: Receive = {

    case Received(data) => receiveData(data)

    case Send(data) => sendData(data)

    case Heartbeat => sendHeartbeat()

    case CloseConnection => sendClose()

    case _: Tcp.ConnectionClosed => Closed()

    case msg => log.info("unknown message: {}", msg)
  }

  override def postStop() {
    scheduler.cancel()

    log.info("Session stop: {}", id)
  }


  def receiveData(data: ByteString) {
    val rawJson = data.utf8String
    val dataJsonEither = decode[TurnToHandleJson](rawJson)

    // Далее по каналу связи доходят только опознанные сообщения
    dataJsonEither match {
      case Right(turnToHandle) =>
        log.info("SERVER session handler: message from client received: {}", turnToHandle.toString)
        taskService ! CommandTask( self, turnToHandle )
      case Left(error) =>
        log.info("SERVER session handler: failed to deserialize message error: {}, msg: {}", error, rawJson)
        connection ! buffer.copy(extraInf = "Deserializing failed. Please, input message again")
    }

  }

  def sendClose(): Unit = {
    connection ! Close
    context.stop(self)
  }

  def sendData(msg: YourTurn): Unit = {
    buffer = msg
    connection ! Write(ByteString(msg.asJson.toString()))
  }

  def sendHeartbeat(): Unit = {
    connection ! Write(ByteString("Ping"))
  }

  def Closed() {
    context.stop(self)
  }
}

object SessionHandler {
  val period: FiniteDuration = 10.seconds

  def props(id: Long, connection: ActorRef): Props = Props(
    new SessionHandler(id, connection)
  )

  // ----- API -----
  final case class Send(data: YourTurn)

  case object CloseConnection

  case object Heartbeat
}