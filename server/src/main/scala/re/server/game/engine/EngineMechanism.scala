package re.server.game.engine

import re.server.game.basicStructures.{BasicUnit, Player, Point, State, StrategicMap}
import re.server.game.units.{Fortress, WarfareUnit}

import scala.annotation.tailrec

trait EngineMechanism {

  def isEmpty(position: Point, currState: State): Boolean = {
    currState.state.foldLeft(true)((acc, player) =>
      !player.warfareUnits.exists(_.position == position)
        && !(player.fortress.position == position)
        && StrategicMap.isInsideMap(position)
        && acc)
  }


  def findUnit(position: Point, player: Player): Option[BasicUnit] =
    player.warfareUnits.find(_.position == position) match {
      case Some(warfareUnit) => Some(warfareUnit)
      case None => if(player.fortress.position == position) Some(player.fortress) else None
    }

  def findPlayer(pos: Point, currState: State): Option[Player] = {
    currState.state.find(_.warfareUnits.map(_.position).contains(pos)) match {
      case Some(player) => Some(player)
      case None => currState.state.find(_.fortress.position == pos)
    }
  }



  def spawnUnits(unitsToSpawn: List[Int], currState: State, actingPlayer: Player): State = {
    @tailrec
    def recRun(state: State, unitsInProduction: List[Int]): State = unitsInProduction match {
      case Nil => state
      case price::tail =>
        val player = state.state.filter(_.id == actingPlayer.id).head
        val updatedPlayer = player.fortress.spawnUnit(price, state) match {
          case None => player
          case Some(spawnedUnit) =>
            if (player.gold >= spawnedUnit.goldCost) {
              player.copy(
                warfareUnits = spawnedUnit :: player.warfareUnits,
                gold = player.gold - spawnedUnit.goldCost
              )
            }
            else player
        }
        recRun(State(state.turnNum, updatedPlayer :: (state.state diff List(player))), tail)
    }
    recRun(currState, unitsToSpawn)
  }

  def moveUnits(unitMoves: List[(Point, Point)], currState: State, actingPlayer: Player): State = {
    @tailrec
    def recRun(state: State, moves: List[(Point, Point)]): State = moves match {
      case Nil => state
      case move::tail =>
        val player = state.state.filter(_.id == actingPlayer.id).head
        val updatedPlayer = findUnit(move._1, player) match {
          case Some(unit) if isEmpty(move._2, currState) && unit.isInstanceOf[WarfareUnit] =>
            val updatedUnit = unit.asInstanceOf[WarfareUnit].move(move._2).getOrElse(unit.asInstanceOf[WarfareUnit])
            player.copy(
              warfareUnits = updatedUnit :: (player.warfareUnits diff List(unit))
            )
          case _ => player
        }
        recRun(State(state.turnNum, updatedPlayer :: (state.state diff List(player))), tail)
    }
    recRun(currState, unitMoves)
  }

  def attackTargets(unitAttacks: List[(Point, Point)], currState: State, actingPlayer: Player): State = {
    def tryToAttack(unitPos: Point, targetPos: Point, player: Player): Option[(Int, BasicUnit)] = {
      findUnit(targetPos, player) match {
        case None => None
        case Some(target) =>
          val targetPlayer = findPlayer(unitPos, currState).getOrElse(player) //TODO исправь getorelse
          findUnit(unitPos, targetPlayer).map(_.asInstanceOf[WarfareUnit]) match {
            // если и цель, и атакующий юнит существуют,
            // тогда атака, иначе ничего
            case Some(unit) => unit.attack(target)
            case None => None
          }
      }
    }

    def getRidOfOtherPlayerAttacks(attacks: List[(Point, Point)], actingPlayer: Player) = {
      attacks.filter{ attack =>
        actingPlayer.warfareUnits.map(_.position).contains(attack._1)
      }
    }

    def getRidOfSameUnitAttacks(attacks: List[(Point, Point)]) = {
      attacks
        .foldLeft(List[(Point, Point)]())((acc, elem) =>
          if (!acc.map(_._1).contains(elem._1)) elem :: acc // если в списке атак нет атак от этого юнита
          else acc
        )
    }

    @tailrec
    def recRun(state: State, attacks: List[(Point, Point)]): State = attacks match {
      case Nil => state
      case attack::tail =>
        val newState = findPlayer(attack._2, state) match {
          case None => state
          case Some(player) =>
            val hpAndUnit = tryToAttack(attack._1, attack._2, player)
            hpAndUnit match {
              case None => state
              case Some(dmgAndUnit) =>
                val dmg = dmgAndUnit._1
                val unit = dmgAndUnit._2//.asInstanceOf[WarfareUnit]
                val stateUpdated = unit match {
                  case warfareUnit : WarfareUnit =>
                    State(
                      state.turnNum,
                      player.copy(
                        warfareUnits =  warfareUnit.copyBasicUnit(warfareUnit.hp - dmg).asInstanceOf[WarfareUnit] ::
                          player.warfareUnits diff List(warfareUnit)
                      ) :: (state.state  diff List(player))
                    )
                  case fortressNew: Fortress => State(
                    state.turnNum,
                    player.copy(
                      fortress = fortressNew.copyBasicUnit(
                        fortressNew.hp - dmg)
                    ) :: (state.state  diff List(player))
                  )
                  case _ => state
                }
                stateUpdated
            }
        }
        recRun(newState, tail)
    }

    val attacksValidated = getRidOfSameUnitAttacks(getRidOfOtherPlayerAttacks(unitAttacks, actingPlayer))
    recRun(currState, attacksValidated)
  }

}
