package re.server.game

import akka.actor.typed.ActorRefResolver
import akka.actor.typed.scaladsl.adapter.ClassicActorSystemOps
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.ask
import akka.actor.ReceiveTimeout
import akka.util.Timeout
import re.server.core.db.StorageHandler
import re.server.core.db.StorageHandler.DBRetrieveState
import re.server.front.SessionHandler.{CloseConnection, Send}
import re.server.game.EngineService._
import re.server.game.basicStructures.{State, _}
import re.server.game.engine.EngineMechanism

import java.util.UUID
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success}

object Engine {
  def props(storageMaker: (ActorRef, UUID)  => Props, maxPlayers: Int): Props =  Props(new Engine(storageMaker, maxPlayers))
}

class Engine(storageMaker: (ActorRef, UUID) => Props ,maxPlayers: Int) extends Actor with ActorLogging with EngineMechanism {

  var players: mutable.ListBuffer[Player] = mutable.ListBuffer.empty
  var playersForNextTurn: mutable.ListBuffer[Player] = mutable.ListBuffer.empty
  val actorRefResolver: ActorRefResolver = ActorRefResolver(context.system.toTyped)
  //val rooms: mutable.HashMap[Long, ActorRef] = mutable.HashMap.empty[Long, ActorRef]

  private val initialGold = 200
  private val workingModTimeout = 120000 milliseconds

  // тестовая коллекция
  var storageService: ActorRef =
    getCollectionInstance(UUID.fromString("00000000-0000-0000-0000-000000000000"))

  // ----- actor -----
  override def preStart() {
    log.info("SERVER engine: starting game service")
  }

  override def receive: Receive = {
  //  case task: SomePlayer => handleAuthenticated(task)
    case task: JoinGameReq =>
      handleJoin(task)

    case GameInProcess  =>
      context.become(workingMod)

    case msg => log.info("SERVER engine: unknown message during lobby mod {}", msg )
  }

  context.setReceiveTimeout(workingModTimeout)
  def workingMod: Receive = {
    case _: JoinGameReq => sender ! YourTurn(0, State.empty, "Game is already in process")

    case task: ExitGameReq =>
    handleExitGameReq(task)

    case task: Turn =>
    log.info("SERVER engine: handling turn")
    handleTurn(sender(), task.turn)

    case task: FallBackToReq =>
    log.info(s"SERVER engine: request to get back to turn #${task.turnNum}")
    handleFallBackToReq(task)

    case ReadyForNewSession =>
      log.info("SERVER engine: ready for a new session")
      context.become(receive)

    case ReceiveTimeout => restartGameSession()

    case msg => log.info("SERVER engine: unknown message during working mod {}", msg )
  }

  override def postStop() {
    log.info("SERVER engine: stopping game service")
  }

  def restartGameSession() = {
    players = ListBuffer.empty
    playersForNextTurn = ListBuffer.empty
    self ! ReadyForNewSession
  }

  def getCollectionInstance(storageUUID: UUID): ActorRef = {
    context.actorOf(storageMaker(self, storageUUID), s"storage-$storageUUID")
  }

  def getPlayer(id: Long, currState: State): Player = currState.state.filter(player => player.id == id).head

  // выбирает игрока, чей ход будет следующим
  def getPlayerForNextTurn(currState: State): Player = {
    if (playersForNextTurn.nonEmpty) {
      val nextPlayer = playersForNextTurn.head
      playersForNextTurn -= nextPlayer
      nextPlayer
    }
    else {
      playersForNextTurn ++= currState.state
      getPlayerForNextTurn(currState)
    }
  }

  // служит для анализа состояния и передачи хода
  def resolveTurnQueue(state: State): Unit = {
    val newTurnNum = if(playersForNextTurn.isEmpty) state.turnNum + 1 else state.turnNum

    val nextPlayer = getPlayerForNextTurn(state)
    if (state.state.count(_.status == InGame) == 1) {
      actorRefResolver.resolveActorRef(nextPlayer.actorRef) ! Send(
        YourTurn(nextPlayer.id, State(newTurnNum, List(nextPlayer.copy(status = Won))), "")
      )
      actorRefResolver.resolveActorRef(nextPlayer.actorRef) ! CloseConnection

      // !! ОБНОВЛЕНИЕ СОСТОЯНИЯ !!
      restartGameSession()
    }
    else actorRefResolver.resolveActorRef(nextPlayer.actorRef) ! Send(YourTurn(nextPlayer.id, state.copy(turnNum = newTurnNum), ""))
  }

  def saveState(state: State): Unit = {
    storageService ! StorageHandler.DBSave(state)
  }

  // ПОВЕДЕНИЕ АКТОРА
  def handleJoin(task: JoinGameReq): State = {
    if (task.currState.state.size == maxPlayers) {
      task.session ! Send(YourTurn(0, State.empty, s"Room is full!"))
      task.currState
    }
    else {
      log.info("SERVER engine: player joins room: " + task.playerId)
      val newPlayer = Player.getDefaultPlayer(players.size, task)

      players += newPlayer
      playersForNextTurn = players // список еще несделавших ход игроков
      task.session ! Send(YourTurn(task.playerId, State.empty,  s"${task.playerName}"))
      val updatedState = players.toList
      if (players.size == maxPlayers) {

        // работа с БД состояний игры
        context.stop(storageService) // остановка актора работы с текущей коллекцией
        val storageUUID: UUID = UUID.randomUUID()
        storageService = getCollectionInstance(storageUUID)

        // работа с игроками
        log.info("SERVER engine: The game has started!")
        val playerForNextTurn = getPlayerForNextTurn(State(1, updatedState))
        val nextTurnJson = YourTurn(playerForNextTurn.id, State(1, updatedState), "")

        saveState(State(1, updatedState))

        val actorRefResolver = ActorRefResolver(context.system.toTyped)
        actorRefResolver.resolveActorRef(playerForNextTurn.actorRef) ! Send(nextTurnJson)
        self ! GameInProcess
      }
      State(1, updatedState)
    }
  }

  def handleExitGameReq(task: ExitGameReq): State = {
    val playerForExit = getPlayer(task.turn.actingPlayerId, task.turn.currState)

    // удаляем из списка игроков
    players -= playerForExit
    // удаляем из кандидатов на следующий ход
    if (playersForNextTurn.contains(playerForExit)) playersForNextTurn -= playerForExit
    actorRefResolver.resolveActorRef(playerForExit.actorRef) ! CloseConnection

    val updatedState = State(task.turn.currState.turnNum, task.turn.currState.state diff List(playerForExit))
    resolveTurnQueue(updatedState)
    updatedState
  }

  def handleFallBackToReq(req: FallBackToReq): Unit = {
    implicit val timeout: Timeout = Timeout(10 seconds)
    (storageService ? DBRetrieveState(req.turnNum))
      .onComplete {
        case Success(state)  =>
          val newState =  state.asInstanceOf[State]
          log.info("SERVER RECEIVED : {}", newState)
          players = ListBuffer()
          players ++= newState.state

          playersForNextTurn = ListBuffer()
          resolveTurnQueue(newState)
        case Failure(exception) =>
          resolveTurnQueue(req.currState)

    }
  }

  def handleTurn(session: ActorRef, task: TurnToHandleJson): State = {
    def getListOfDefeated(currState: State) = {
      val lstOfDefeated = for (player <- currState.state if player.fortress.hp <= 0) yield player
      State(currState.turnNum, lstOfDefeated)
    }

    // после производства
    val actingPlayer = getPlayer(task.actingPlayerId, task.currState)
    val stateAfterSpawn = spawnUnits(task.fortressSpawn, task.currState, actingPlayer)
  //  log.info("SERVER engine: units after spawn: {}",
  //    stateAfterSpawn.state.filter(_.id==actingPlayer.id).head.warfareUnits)
    // после ходов
    val stateAfterMoves = moveUnits(
    task.unitMoves, stateAfterSpawn, stateAfterSpawn.state.filter(_.id==actingPlayer.id).head
  )
  //  log.info("SERVER engine: units after movement: {}"
  //    , stateAfterMoves.state.filter(_.id==actingPlayer.id).head.warfareUnits)
    // после атак
    val stateAfterUnitAttacks = attackTargets(
    task.unitAttacks, stateAfterMoves, stateAfterMoves.state.filter(_.id==actingPlayer.id).head
  )
  //  log.info("SERVER engine: units after attacks: {}",
  //    stateAfterUnitAttacks)//.filter(_.id==actingPlayer.id).head.warfareUnits)
    // после атак по крепостям
    val stateAfterUnitAttacksOnFortresses = attackTargets(
    task.unitAttacksOnFortresses, stateAfterUnitAttacks, stateAfterUnitAttacks.state.filter(_.id==actingPlayer.id).head
  )
  //  log.info("SERVER engine: fortress after attacks: {}",
  //    stateAfterUnitAttacksOnFortresses.state.filter(_.id==actingPlayer.id).head.fortress)
    // после смертей юнитов
    val stateAfterUnitsDeaths = State(
    task.currState.turnNum,
    for (player <- stateAfterUnitAttacksOnFortresses.state) yield {
      val newWarfareUnitsList = player.warfareUnits.filter(_.hp > 0)
      player.copy(warfareUnits = newWarfareUnitsList)
    }
  )
    log.info("SERVER engine: units after deaths: {}",
      stateAfterUnitsDeaths.state.filter(_.id==actingPlayer.id).head.warfareUnits)

    val defeatedPlayers = getListOfDefeated(stateAfterUnitsDeaths)
    val stateAfterDefeats =
      State(task.currState.turnNum, stateAfterUnitsDeaths.state diff defeatedPlayers.state)

    for (defeatedPlayer <- defeatedPlayers.state) {
      val updatedState = defeatedPlayer.copy(status = Lost) :: stateAfterDefeats.state

      players -= defeatedPlayer
      if (playersForNextTurn.contains(defeatedPlayer)) playersForNextTurn -= defeatedPlayer
      actorRefResolver.resolveActorRef(defeatedPlayer.actorRef) ! Send(YourTurn(defeatedPlayer.id, State(task.currState.turnNum, updatedState), ""))
      actorRefResolver.resolveActorRef(defeatedPlayer.actorRef) ! CloseConnection
    }

    if (playersForNextTurn.isEmpty) saveState(
      stateAfterDefeats.copy(turnNum = task.currState.turnNum + 1)
    ) // Инкремент для сохранения

    resolveTurnQueue(stateAfterDefeats)

    stateAfterDefeats
  }

}

object EngineService {

  // ----- API -----
  final case class JoinGameReq(session: ActorRef, playerId: Long, playerName: String, currState: State)
  final case class ExitGameReq(session: ActorRef, turn: TurnToHandleJson)
  final case class Turn(session: ActorRef, turn: TurnToHandleJson)
  final case class FallBackToReq(session: ActorRef, turnNum: Int, currState: State)
  case object GameInProcess
  case object ReadyForNewSession
}