package re.server.game.units

import re.server.game.basicStructures.{BasicUnit, Point, StrategicMap}
import io.circe.{Decoder, Encoder}
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.semiauto.{deriveConfiguredDecoder, deriveConfiguredEncoder}



sealed trait WarfareUnit extends BasicUnit {

  val attackDamage: Int
  val attackDistance: Int

  val goldCost: Int
  val moveDistance: Int

  def move(newPosition: Point): Option[WarfareUnit] = {
    if (position.getDistance(newPosition) <= moveDistance && StrategicMap.isInsideMap(newPosition))
      Some(this.copyBasicUnit(newPosition).asInstanceOf[WarfareUnit])
    else None
  }

  // возвращает урон по юниту в точке
  // такая штука нужна для накопления урона от разных юнитов
  def attack(target: BasicUnit): Option[(Int, BasicUnit)] = {
    if (position.getDistance(target.position) <= attackDistance
      && position.getDistance(target.position) > 0) {
      val newWarfareUnitState = target.copyBasicUnit(target.hp) // - attackDamage)
      Some((attackDamage, newWarfareUnitState))
    }
    else None
  }
}

object WarfareUnit {
  private implicit final val customConfig: Configuration = Configuration.default.withDefaults
  implicit final val WarfareJsonDecoder: Decoder[WarfareUnit] = deriveConfiguredDecoder
  implicit final val WarfareJsonEncoder: Encoder[WarfareUnit] = deriveConfiguredEncoder
}

case class Swordsman(hp: Int, position: Point) extends WarfareUnit {
  override val attackDamage = 2
  override val attackDistance = 1
  override val moveDistance = 2
  override val goldCost = 30

  override def copyBasicUnit(newHp: Int): WarfareUnit = copy(hp = newHp)
  override def copyBasicUnit(newPosition: Point): WarfareUnit = copy(position = newPosition)

}

object Swordsman {
  val defaultHp = 3
  val unitCode = 1
}

case class Archer(hp: Int, position: Point) extends WarfareUnit {
  override val attackDamage = 1
  override val attackDistance = 3
  override val moveDistance = 1
  override val goldCost = 40

  override def copyBasicUnit(newHp: Int): WarfareUnit = copy(hp = newHp)
  override def copyBasicUnit(newPosition: Point): WarfareUnit = copy(position = newPosition)
}

object Archer {
  val defaultHp = 1
  val unitCode = 2
}
