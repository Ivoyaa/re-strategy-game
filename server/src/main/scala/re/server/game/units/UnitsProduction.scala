package re.server.game.units

import re.server.game.basicStructures.{BasicUnit, Point}
import re.server.game.basicStructures.{StrategicMap, State}
import io.circe.{Decoder, Encoder}
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.semiauto.{deriveConfiguredDecoder, deriveConfiguredEncoder}

sealed trait Factory extends BasicUnit {
  def spawnUnit(unitCode: Int, currState: State): Option[WarfareUnit]
}


case class Fortress(position: Point, hp: Int) extends Factory {
  def spawnUnit(unitCode: Int, currState: State): Option[WarfareUnit] = {
    val occupiedPositionsUnits: List[Point] = for (
      player <- currState.state;
      warfareUnit <- player.warfareUnits
    ) yield { warfareUnit.position }

    val occupiedPositionsFortresses: List[Point] = for (
      player <- currState.state
    ) yield { player.fortress.position }

    val occupiedPositions = occupiedPositionsUnits ::: occupiedPositionsFortresses

    val pointsToOccupy = for (
      x <- -1 to 1;
      y <- -1 to 1
      if (x,y) != (0,0))
    yield Point(position.x + x, position.y + y)

    // drop нужен для учета позиций, уже занятых произведенными на этом ходу юнитами
    pointsToOccupy.find(point =>
      !occupiedPositions.contains(point) && StrategicMap.isInsideMap(point)) match {
      case None => None
      case Some(spawnPosition) => unitCode match {
        case 1 => Some(Swordsman(Swordsman.defaultHp,spawnPosition))
        case 2 => Some(Archer(Archer.defaultHp, spawnPosition))
      }
    }
  }

  override def copyBasicUnit(newHp: Int): Fortress = copy(hp = newHp)
  override def copyBasicUnit(newPosition: Point): Fortress = copy(position = newPosition)
}

object Fortress {
  val defaultHp = 20

  private implicit final val customConfig: Configuration = Configuration.default.withDefaults
  implicit final val FortressJsonDecoder: Decoder[Fortress] = deriveConfiguredDecoder
  implicit final val FortressJsonEncoder: Encoder[Fortress] = deriveConfiguredEncoder
}

