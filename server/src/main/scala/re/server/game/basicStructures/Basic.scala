package re.server.game.basicStructures

import io.circe.{Decoder, Encoder}
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.semiauto.{deriveConfiguredDecoder, deriveConfiguredEncoder}
import re.server.command._

import math.abs

sealed case class Point(x: Int, y: Int) {
  def getDistance(anotherPoint: Point): Int = {
    abs(x - anotherPoint.x) + abs(y - anotherPoint.y) // манхэттенская метрика
  }
}

object Point {
  private implicit final val customConfig: Configuration = Configuration.default.withDefaults
  implicit final val pointDecoder: Decoder[Point] = deriveConfiguredDecoder
  implicit final val pointEncoder: Encoder[Point] = deriveConfiguredEncoder
}

trait BasicUnit {
  val hp: Int
  val position: Point

  def copyBasicUnit(newHp: Int): BasicUnit
  def copyBasicUnit(newPosition: Point): BasicUnit
}

final case class State(turnNum: Int, state: List[Player])
object State {
  private implicit final val customConfig: Configuration = Configuration.default.withDefaults
  implicit final val stateJsonDecoder: Decoder[State] = deriveConfiguredDecoder
  implicit final val stateJsonEncoder: Encoder[State] = deriveConfiguredEncoder

  def empty: State = State(0, List.empty)
}

sealed case class TurnToHandleJson(actingPlayerId: Long
                            ,command: GameMessage
                            ,currState: State
                            ,unitAttacks: List[(Point, Point)]
                            ,unitAttacksOnFortresses: List[(Point, Point)]
                            ,unitMoves: List[(Point, Point)]
                            ,fortressSpawn: List[Int]
                            ,extraInfo: String) //служит для сообщения ника, передачи ошибки, хода для отката

object TurnToHandleJson {
  private implicit final val customConfig: Configuration = Configuration.default.withDefaults
  implicit final val turnToHandleJsonDecoder: Decoder[TurnToHandleJson] = deriveConfiguredDecoder
  implicit final val turnToHandleJsonJsonEncoder: Encoder[TurnToHandleJson] = deriveConfiguredEncoder

  def empty: TurnToHandleJson =
    TurnToHandleJson(0, NoSpecialCommand, State.empty, List.empty, List.empty, List.empty, List.empty, "" )
  def empty(error: Exception): TurnToHandleJson =
    TurnToHandleJson(0, NoSpecialCommand, State.empty, List.empty, List.empty, List.empty, List.empty, error.toString )
  def empty(cmd: GameMessage): TurnToHandleJson =
    TurnToHandleJson(0, cmd, State.empty, List.empty, List.empty, List.empty, List.empty, "" )
}


sealed case class YourTurn(playerId: Long, currState: State, extraInf: String)

object YourTurn {
  private implicit final val customConfig: Configuration = Configuration.default.withDefaults
  implicit final val yourTurnJsonDecoder: Decoder[YourTurn] = deriveConfiguredDecoder
  implicit final val yourTurnJsonEncoder: Encoder[YourTurn] = deriveConfiguredEncoder
}






