package re.server.game.basicStructures

import io.circe.Codec
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.semiauto.deriveEnumerationCodec

sealed trait PlayerStatus
case object InGame extends PlayerStatus
case object Lost extends  PlayerStatus
case object Won extends PlayerStatus

object PlayerStatus {
  private implicit final val customConfig: Configuration = Configuration.default.withDefaults
  implicit val playerStatusCodec: Codec[PlayerStatus] = deriveEnumerationCodec[PlayerStatus]
}





