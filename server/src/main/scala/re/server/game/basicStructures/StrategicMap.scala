package re.server.game.basicStructures

object StrategicMap {
  private val width = 10
  private val height = 10

  val cornersToSpawn: Vector[Point] = Vector(Point(2,2), Point(8,2), Point(2,8), Point(8,8))

  def isInsideMap(point: Point): Boolean =
    point.x > 0 && point.x <= width && point.y > 0 && point.y <= height
}
