package re.server.game.basicStructures

import akka.serialization.Serialization
import re.server.game.EngineService.JoinGameReq
import re.server.game.units.{Fortress, WarfareUnit}
import io.circe.{Decoder, Encoder}
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.semiauto.{deriveConfiguredDecoder, deriveConfiguredEncoder}

case class Player(id: Long
                         ,actorRef: String
                         ,status: PlayerStatus
                         ,name: String
                         ,gold: Int
                         ,fortress: Fortress
                         ,warfareUnits: List[WarfareUnit])

object Player {

  def getDefaultPlayer(numOfPlayers: Int, task: JoinGameReq): Player = Player (
    task.playerId
    ,Serialization.serializedActorPath(task.session)
    ,InGame
    ,task.playerName
    ,initialGold
    ,Fortress( StrategicMap.cornersToSpawn(numOfPlayers) ,Fortress.defaultHp)
    ,List.empty//,List(Swordsman(3,Point(1,1)), Swordsman(3,Point(1,2)), Swordsman(3, Point(1,3)))
  )

  private val initialGold = 200

  private implicit final val customConfig: Configuration = Configuration.default.withDefaults
  implicit final val PlayerJsonDecoder: Decoder[Player] = deriveConfiguredDecoder
  implicit final val PlayerJsonEncoder: Encoder[Player] = deriveConfiguredEncoder
}