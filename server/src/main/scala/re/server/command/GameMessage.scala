package re.server.command

import io.circe.Codec
import io.circe.generic.extras.semiauto.deriveEnumerationCodec

sealed trait GameMessage
case object NoSpecialCommand extends GameMessage
case object JoinGame extends GameMessage
case object EndTurn  extends GameMessage
case object ExitRoom extends GameMessage
// стоило бы сделать в виде кейс-класса, однако тогда сериализация выдает плоховыглядящий json
// подразумевается, что код возврата к ходу будет передаваться в поле extaInf класса TurnToHandle
case object FallbackToTurn extends GameMessage




object GameMessage {
  implicit val gameMessageCodec: Codec[GameMessage] = deriveEnumerationCodec[GameMessage]
}
