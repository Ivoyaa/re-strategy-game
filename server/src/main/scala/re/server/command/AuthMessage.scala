package re.server.command

sealed trait AuthMessage { def code: Int }

// в текущий момент не используются
case object AuthCommand {
  case object Ping     extends AuthMessage { val code = 1 }
  case object Auth     extends AuthMessage { val code = 2 }
  case object AuthResp extends AuthMessage { val code = 3 }
  case object AuthErr  extends AuthMessage { val code = 4 }
  case object Join     extends AuthMessage { val code = 5 }
}