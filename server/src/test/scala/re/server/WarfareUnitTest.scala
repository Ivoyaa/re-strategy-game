package re.server

import org.scalatest.funsuite.AnyFunSuite
import re.server.game.basicStructures.Point
import re.server.game.units.{Archer, Fortress, Swordsman}

class WarfareUnitTest extends AnyFunSuite{
  val swordsman: Swordsman = Swordsman(3, Point(3,3))
  val swordsmanNeighbor: Swordsman = Swordsman(3, Point(2,3))
  val swordsmanFar: Swordsman = Swordsman(3, Point(swordsman.position.x + swordsman.attackDistance + 1,3))
  val archer: Archer = Archer(3, Point(3,3))
  val swordsmanOnEdge: Swordsman = Swordsman(3, Point(1,1))
  val fortress: Fortress = Fortress(Point(3,4), 20)

  test("WarfareUnit.move") {

    // юнит может переместиться на минимальную дистанцию
    assert(
      swordsman.move(Point(3,2)) === Some(Swordsman(3, Point(3,2)))
    )

    // юнит может переместиться на максимальную дистанцию
    assert(
      swordsman.move(Point(3 + swordsman.moveDistance, 3))
      === Some(Swordsman(3, Point(3 + swordsman.moveDistance, 3)))
    )

    // при передвижении на дистанцию, большую возможной, выдается None
    assert(
      swordsman.move(Point(3 + swordsman.moveDistance + 1, 3))
        === None
    )

    // при передвижении за границу карты выдается None
    assert(
      swordsmanOnEdge.move(Point(0,1))
        === None
    )
  }

  test("WarfareUnit.attack") {
    // юнит атакует рядом стоящего юнита и наносит урон
    assert(
      swordsman.attack(swordsmanNeighbor) === Some((swordsman.attackDamage, swordsmanNeighbor))
    )

    // при атаке юнита за пределами радиуса атаки атакующего юнита возвращается None
    assert(
      swordsman.attack(swordsmanFar) === None
    )

    // юнит атакует стоящего не рядом (но в пределах досягаемости атаки) юнита и наносит урон
    assert(
      archer.attack(swordsmanFar) === Some((1,swordsmanFar))
    )

    // юнит атакует крепость и наносит урон
    assert(
      swordsman.attack(fortress) === Some((2, fortress))
    )

  }
}
