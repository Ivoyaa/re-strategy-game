package re.server

import org.scalatest.funsuite.AnyFunSuite
import re.server.game.basicStructures.{InGame, Player, Point, State, StrategicMap}
import re.server.game.engine.EngineMechanism
import re.server.game.units.{Archer, Fortress, Swordsman}


class EngineMechanismTest extends AnyFunSuite with EngineMechanism {

  val emptyState: State = State.empty
  val swordsmanTest: Swordsman = Swordsman(Swordsman.defaultHp, Point(0,0))
  val archerTest: Archer = Archer(Archer.defaultHp, Point(0,0))

  // Fortress in (2,2) cell
  val oneFortressState: State =
    State(0, List(Player(0,"",InGame,"",200,Fortress(Point(2,2),20),List.empty)))

  // Warfare unit in (2,2) cell
  val oneWarfareUnitState: State =
    State(0,
      List(Player(0,"",InGame,"",200,Fortress(Point(-1,-1),20),List(Swordsman(Swordsman.defaultHp, Point(2,2)))))
    )

  // Warfare unit on the edge in (1,1) cell
  val oneEdgeWarfareUnitState: State =
    State(0,
      List(Player(0,"",InGame,"",200,Fortress(Point(-1,-1),20),List(Swordsman(Swordsman.defaultHp, Point(1,1)))))
    )

  // Warfare units of the same player in cells (2,2) and (2,3)
  val twoWarfareUnitSamePlayersState: State =
    State(0,
      List(Player(0,"",InGame,"",200,Fortress(Point(-1,-1),20)
        ,List(Swordsman(3, Point(2,2)), Swordsman(Swordsman.defaultHp, Point(2,3)))))
    )

  // Warfare units of different players in cells (2,2) and (2,3)
  val twoWarfareUnitDiffPlayersState: State = oneWarfareUnitState.copy(
    state = oneWarfareUnitState.state
      ++ List(Player(1,"",InGame,"",200,Fortress(Point(-1,-1),20),List(Swordsman(Swordsman.defaultHp, Point(2,3)))))
  )

  test("EngineMechanism.isEmpty") {
    assert(isEmpty(Point(2,2), oneFortressState) === false)
    assert(isEmpty(Point(2,3), oneFortressState) === true)
    assert(isEmpty(Point(2,2), oneWarfareUnitState) === false)
  }

  test("EngineMechanism.findUnit") {
    assert(findUnit(Point(2,2), oneFortressState.state.head) === Some(Fortress(Point(2,2), 20)))
    assert(findUnit(Point(2,3), oneFortressState.state.head) === None)
    assert(findUnit(Point(2,2), oneWarfareUnitState.state.head) === Some(Swordsman(3,Point(2,2))))
    assert(findUnit(Point(2,3), oneWarfareUnitState.state.head) === None)
  }

  test("EngineMechanism.findPlayer") {
    assert(findPlayer(Point(2,2), oneFortressState)
      === Some(Player(0,"",InGame,"",200,Fortress(Point(2,2),20),List.empty)))
    assert(findPlayer(Point(2,3), oneFortressState) === None)
  }

  test("EngineMechanism.spawnUnits") {
    assert(
      spawnUnits(
        List.empty
        ,oneFortressState
        ,oneFortressState.state.head
      ) === oneFortressState
    )

    // проверяет появление юнита в списке юнитов игрока, без уточнения его местонахождения
    assert(
      spawnUnits(
        List(Swordsman.unitCode)
        ,oneFortressState
        ,oneFortressState.state.head
      ).state.head.warfareUnits.map(_.copyBasicUnit(Point(0,0)))
        === List(Swordsman(Swordsman.defaultHp, Point(0,0)))
    )

    // проверяет факт, что игрок, имеющий деньги только на одного юнита определенного типа,
    // не может произвести больше одного юнита этого типа
    val poorPlayerState =
      State(0, List(Player(0,"",InGame,"",Swordsman(3,Point(1,1)).goldCost,Fortress(Point(2,2),20),List.empty)))
    assert(
      spawnUnits(
        List(Swordsman.unitCode, Swordsman.unitCode, Swordsman.unitCode)
        ,poorPlayerState
        ,poorPlayerState.state.head
      ).state.head.warfareUnits.map(_.copyBasicUnit(Point(0,0)))
        === List(Swordsman(Swordsman.defaultHp, Point(0,0)))
    )

    val testUnits = {for (
      x <- 1 to 3;
      y <- 1 to 3
      if (x,y) != (2,2)
    ) yield Swordsman(3, Point(x,y))}.toList

    val fullyOccupiedPlayerState =
      State(0, List(Player(0,"",InGame,"",200,Fortress(Point(2,2),20), testUnits)))
    // проверяет факт, что если вокруг крепости все клетки заняты, то произвести юнита нельзя
    assert(
      spawnUnits(
        List(Swordsman.unitCode)
        ,fullyOccupiedPlayerState
        ,fullyOccupiedPlayerState.state.head
      ).state.head.warfareUnits.size
        === testUnits.size
    )
    // проверяет, что золото не потратилось в случае отмены производства при всех занятых клетках вокруг
    assert(
      spawnUnits(
        List(Swordsman.unitCode)
        ,fullyOccupiedPlayerState
        ,fullyOccupiedPlayerState.state.head
      ).state.head.gold
        === fullyOccupiedPlayerState.state.head.gold
    )

    val unitCodesForTestProduction = {for (_ <- 1 to 20) yield Swordsman.unitCode}.toList
    val richPlayerState =
      State(0, List(Player(0,"",InGame,"",9999,Fortress(Point(2,2),20),List.empty)))
    val richPlayerOnTheEdgeState =
      State(0, List(Player(0,"",InGame,"",9999,Fortress(Point(1,1),20),List.empty)))

    // проверяет, что можно произвести максимальное количество юнитов (при наличии свободных клеток)
    // но не больше, чем количество свободных клеток
    assert(
      spawnUnits(
        unitCodesForTestProduction
        ,richPlayerState
        ,richPlayerState.state.head
      ).state.head.warfareUnits.size
        === 8 // <- количество клеток вокруг какой либо неграничной клетки
    )

    assert(
      spawnUnits(
        unitCodesForTestProduction
        ,richPlayerOnTheEdgeState
        ,richPlayerOnTheEdgeState.state.head
      ).state.head.warfareUnits.size
        === 3 // <- количество клеток вокруг угловой граничной клетки
    )
  }

  test ("EngineMechanism.moveUnits") {

    // при попытке сходить пустой клеткой - ничего не происходит
    assert(
      moveUnits(
        List((Point(4,2), Point(5,2)))
        ,oneFortressState
        ,oneFortressState.state.head
      ) === oneFortressState
    )

    // при попытке сходить крепостью - ничего не происходит
    assert(
      moveUnits(
        List((Point(2,2), Point(3,2)))
        ,oneFortressState
        ,oneFortressState.state.head
      ) === oneFortressState
    )

    // можно сходить юнитом на минимальную дистанцию
    assert(
      moveUnits(
        List((Point(2,2), Point(3,2)))
        ,oneWarfareUnitState
        ,oneWarfareUnitState.state.head
      ).state.head.warfareUnits === List(Swordsman(Swordsman.defaultHp, Point(3,2)))
    )

    // при движении на дистанцию, большую максимальной для юнита - ничего не происходит
    val maxDistance = Swordsman(3, Point(2,2)).moveDistance
    assert(
      moveUnits(
        List((Point(2,2), Point(2 + maxDistance + 1, 2)))
        ,oneWarfareUnitState
        ,oneWarfareUnitState.state.head
      ).state.head.warfareUnits === List(Swordsman(Swordsman.defaultHp, Point(2,2)))
    )

    // проверяет, что при попытке передвинуть юнит за границу карты - ничего не происходит
    assert(
      moveUnits(
        List((Point(1,1), Point(0,1)))
        ,oneEdgeWarfareUnitState
        ,oneEdgeWarfareUnitState.state.head
      ) == oneEdgeWarfareUnitState
    )



    // проверяет, что при попытке походить своим юнитом и чужим
    // свой юнит должен переместиться, чужой остаться на месте
    assert(
      moveUnits(
        List((Point(2,2), Point(3,2)), (Point(4,2), Point(3,3)))
        ,twoWarfareUnitDiffPlayersState
        ,twoWarfareUnitDiffPlayersState.state.filter(_.id == 0).head
      ).state.map(_.warfareUnits)
      === List(List(Swordsman(Swordsman.defaultHp, Point(3,2))), List(Swordsman(Swordsman.defaultHp, Point(2,3))))
      ||
        moveUnits(
          List((Point(2,2), Point(3,2)), (Point(2,3), Point(3,3)))
          ,twoWarfareUnitDiffPlayersState
          ,twoWarfareUnitDiffPlayersState.state.filter(_.id == 0).head
        ).state.map(_.warfareUnits)
          === List(List(Swordsman(Swordsman.defaultHp, Point(2,3))), List(Swordsman(Swordsman.defaultHp, Point(3,2))))
    )


    // проверяет, что при движении двух юнитов одного игрока (действующего) оба юнита перемещаются
    assert(
      moveUnits(
        List((Point(2, 2), Point(3, 2)), (Point(2, 3), Point(3, 3)))
        , twoWarfareUnitSamePlayersState
        , twoWarfareUnitSamePlayersState.state.filter(_.id == 0).head
      ).state.flatMap(_.warfareUnits)
      === List(Swordsman(3,Point(3,2)), Swordsman(3,Point(3,3)))
      ||
        moveUnits(
          List((Point(2, 2), Point(3, 2)), (Point(2, 3), Point(3, 3)))
          , twoWarfareUnitSamePlayersState
          , twoWarfareUnitSamePlayersState.state.filter(_.id == 0).head
        ).state.flatMap(_.warfareUnits)
          === List(Swordsman(3,Point(3,3)), Swordsman(3,Point(3,2)))
    )
  }

  test ("EngineMechanism.attackTargets") {

    // при атаке пустой клектой пустую клетку ничего не происходит
    assert(
      attackTargets(
        List((Point(4,2), Point(5,2)))
        ,oneFortressState
        ,oneWarfareUnitState.state.head
      ) === oneFortressState
    )

    // при атаке пустой клеткой другой юнит (крепость) ничего не происходит
    assert(
      attackTargets(
        List((Point(3,2), Point(2,2)))
        ,oneFortressState
        ,oneWarfareUnitState.state.head
      ) === oneFortressState
    )

    // при атаке на пустую клетку боевым юнитом ничего не происходит
    assert(
      attackTargets(
        List((Point(2,2), Point(3,2)))
        ,oneWarfareUnitState
        ,oneWarfareUnitState.state.head
      ) === oneWarfareUnitState
    )

    // при атаке на пустую клетку крепостью ничего не происходит
    assert(
      attackTargets(
        List((Point(2,2), Point(3,2)))
        ,oneFortressState
        ,oneFortressState.state.head
      ) === oneFortressState
    )

    // при атаке юнитом одного игрока юнита другого игрока, целевой юнит теряет жизни, равные урону юнита
    assert(
      attackTargets(
        List((Point(2,2), Point(2,3)))
        ,twoWarfareUnitDiffPlayersState
        ,twoWarfareUnitDiffPlayersState
          .state.find(_.warfareUnits.map(_.position).contains(Point(2,2))).get
      ).state.flatMap(_.warfareUnits)
        === List(
        Swordsman(Swordsman.defaultHp, Point(2,2))
        ,Swordsman(Swordsman.defaultHp - swordsmanTest.attackDamage, Point(2,3))
      )
      ||
        attackTargets(
          List((Point(2,2), Point(2,3)))
          ,twoWarfareUnitDiffPlayersState
          ,twoWarfareUnitDiffPlayersState
            // действующим игроком выбирается юнит, наносящий удар
            .state.find(_.warfareUnits.map(_.position).contains(Point(2,2))).get
        ).state.flatMap(_.warfareUnits)
          === List(
          Swordsman(Swordsman.defaultHp - swordsmanTest.attackDamage, Point(2,3))
          ,Swordsman(Swordsman.defaultHp, Point(2,2))
        )
    )

    // при атаке юнитом, не принадлежащим игроку, юнита другого игрока, целевой юнит не теряет жизни
    assert(
      attackTargets(
        List((Point(2,2), Point(2,3)))
        ,twoWarfareUnitDiffPlayersState
        ,twoWarfareUnitDiffPlayersState
          // действующим игроком выбирается игрок, чей юнит под ударом
          .state.find(_.warfareUnits.map(_.position).contains(Point(2,3))).get
      ).state.flatMap(_.warfareUnits)
        === List(
        Swordsman(Swordsman.defaultHp, Point(2,2))
        ,Swordsman(Swordsman.defaultHp, Point(2,3))
      )
        ||
        attackTargets(
          List((Point(2,2), Point(2,3)))
          ,twoWarfareUnitDiffPlayersState
          ,twoWarfareUnitDiffPlayersState
          .state.find(_.warfareUnits.map(_.position).contains(Point(2,3))).get
        ).state.flatMap(_.warfareUnits)
          === List(
          Swordsman(Swordsman.defaultHp, Point(2,3))
          ,Swordsman(Swordsman.defaultHp, Point(2,2))
        )
    )

    val twoEnemyTwoFriendlyUnitsState = State(0,List(
      Player(1,"",InGame,"",200,Fortress(Point(-1,-1),20),
        List(Swordsman(Swordsman.defaultHp, Point(2,3)), Archer(Archer.defaultHp, Point(4,3))))
      ,Player(0,"",InGame,"",200,Fortress(Point(-1,-1),20),
        List(Swordsman(Swordsman.defaultHp, Point(3,3)), Archer(Archer.defaultHp, Point(5,3))))
    ))
    val twoEnemyTwoFriendlyUnitsAfterAttacksState = State(0,List(
      Player(1,"",InGame,"",200,Fortress(Point(-1,-1),20),
        List(Swordsman(Swordsman.defaultHp, Point(2,3)), Archer(Archer.defaultHp, Point(4,3))))
      ,Player(0,"",InGame,"",200,Fortress(Point(-1,-1),20),
        List(Swordsman(Swordsman.defaultHp - swordsmanTest.attackDamage, Point(3,3)),
          Archer(Archer.defaultHp - archerTest.attackDamage, Point(5,3))))
    )).state.toSet[Player].map(x => x.warfareUnits.toSet) // преобразования в сеты, чтобы порядок следования не влиял

    assert(
      attackTargets(
        List((Point(2,3), Point(3,3)), (Point(4,3), Point(5,3)))
        ,twoEnemyTwoFriendlyUnitsState
        ,twoEnemyTwoFriendlyUnitsState.state.find(_.warfareUnits.map(_.position).contains(Point(2,3))).get
      ).state.toSet[Player].map(x => x.warfareUnits.toSet) ===  twoEnemyTwoFriendlyUnitsAfterAttacksState
    )

  }
}
