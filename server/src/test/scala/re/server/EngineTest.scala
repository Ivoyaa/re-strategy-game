package re.server

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, _}
import re.server.command.{EndTurn, ExitRoom}
import re.server.front.SessionHandler.Send
import re.server.game.Engine
import re.server.game.EngineService.{ExitGameReq, FallBackToReq, JoinGameReq, Turn}
import re.server.game.basicStructures._
import re.server.game.units.Fortress

import java.util.UUID
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{Duration, DurationInt}

class EngineTest()
  extends TestKit(ActorSystem("serverTest"))
    with ImplicitSender
    with wordspec.AnyWordSpecLike
    with  org.scalatest.matchers.should.Matchers
    with BeforeAndAfterAll {

  def storageTestMaker: ActorRef => (ActorRef, UUID) => Props
  = testProbeRef => (engineRef, uuid) => StorageMock.props(engineRef, uuid, testProbeRef)

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  // !!! Engine внутри создает экземпляр StorageService !!!
  // стоит либо предоставить экземпляр монги для стандартного StorageService, либо использовать актор-заглушку
  "An Engine actor" should {

    //проверка на запрос на подключение
    "send message Send(YourTurn) as response to JoinGameReq" in {
      val testProbe = TestProbe()
      val engine = system.actorOf(Engine.props(storageTestMaker(testProbe.ref),2))
      engine ! JoinGameReq(self, 1, "", State.empty)
      expectMsg(Send(YourTurn(1,State.empty,"")))
    }

    //проверка на старт игры при наборе достаточного числа игроков
    "send message Send(YourTurn) with current turnNum = 1 as response to fulfilling room" in {
      val testProbe = TestProbe()
      val engine = system.actorOf(Engine.props(storageTestMaker(testProbe.ref),2))
      engine ! JoinGameReq(self, 1, "", State.empty)
      engine ! JoinGameReq(self, 2, "", State.empty)
      engine ! JoinGameReq(self, 3, "", State.empty)
      engine ! JoinGameReq(self, 4, "", State.empty)
      fishForMessage(Duration(500, "millis"))(
        f = {
          case Send(y) if y.currState.turnNum == 1 => true
          case _ => false
        }
      )
    }

    //проверка на реагирование на выход из игры
    "send  Send(YourTurn) as response to ExitRoom request" in {
      val testProbe = TestProbe()
      val engine = system.actorOf(Engine.props(storageTestMaker(testProbe.ref),2))
      engine ! JoinGameReq(self, 1, "", State.empty)
      engine ! JoinGameReq(self, 2, "", State.empty)
      val exitRoomTest = TurnToHandleJson(
        1, ExitRoom, State(1,
          List(
            Player(1,"",InGame,"",100,Fortress(Point(1,1),20),List.empty)
            ,Player(2,"",InGame,"",100,Fortress(Point(2,2),20),List.empty)
          )
        )
        ,List.empty, List.empty, List.empty, List.empty, ""
      )

      engine ! ExitGameReq(self, exitRoomTest)
      expectMsgClass(classOf[Send])
    }

    "send  Send(YourTurn) as response to Turn (end of turn of a player)" in {
      val testProbe = TestProbe()
      val engine = system.actorOf(Engine.props(storageTestMaker(testProbe.ref),2))
      engine ! JoinGameReq(self, 1, "", State.empty)
      engine ! JoinGameReq(self, 2, "", State.empty)
      val turnTest = TurnToHandleJson(
        1, EndTurn, State(1,
          List(
            Player(1,"",InGame,"",100,Fortress(Point(1,1),20),List.empty)
            ,Player(2,"",InGame,"",100,Fortress(Point(2,2),20),List.empty)
          )
        )
        ,List.empty, List.empty, List.empty, List.empty, ""
      )

      engine ! Turn(self, turnTest)
      expectMsgClass(classOf[Send])
    }

    "send DBRetrieve to StorageMock as response to FallBackToTurn request" in {
      val testProbe = TestProbe()
      val engine = system.actorOf(Engine.props(storageTestMaker(testProbe.ref),2))
      engine ! JoinGameReq(self, 1, "", State.empty)
      engine ! JoinGameReq(self, 2, "", State.empty)
      //нужно время на смену поведения актора в рабочий режим
      system.scheduler.scheduleOnce(300 millis, engine, FallBackToReq(self, 3, State.empty))
      testProbe.fishForMessage(Duration(500, "millis"))(
        f = {
          case "OK REQUEST FOR STATE" => true
          case _ => false
        }
      )
    }

    "send DBSave to StorageMock as response to FallBackToTurn request" in {
      val testProbe = TestProbe()
      val engine = system.actorOf(Engine.props(storageTestMaker(testProbe.ref),2))
      engine ! JoinGameReq(self, 1, "", State.empty)
      engine ! JoinGameReq(self, 2, "", State.empty)
      testProbe.expectMsg("OK SAVED")
    }
  }
}
