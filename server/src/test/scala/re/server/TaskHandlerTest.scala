package re.server

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestActors, TestKit}
import org.scalatest.{BeforeAndAfterAll, _}
import re.server.command.{EndTurn, ExitRoom, FallbackToTurn, JoinGame}
import re.server.core.TaskHandler
import re.server.core.TaskHandler.CommandTask
import re.server.game.EngineService
import re.server.game.basicStructures.TurnToHandleJson

import scala.concurrent.duration.Duration




class TaskHandlerTest()
  extends TestKit(ActorSystem("server"))
    with ImplicitSender
    with wordspec.AnyWordSpecLike
    with  org.scalatest.matchers.should.Matchers
    with BeforeAndAfterAll {

  // так как внутрь TaskHandler-а заложен путь, куда он должен дальше посылать сообщения для обработки,
  // приходиться имитировать нахождения актора с именем game по этому пути
  val mockActor = system.actorOf(TestActors.forwardActorProps(self), "game")

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A TaskHandler actor" should {


    // NoSpecialCommand
    "send no message  in response to TurnToHandle with NoSpecialCommand msg " in {
      val echo = system.actorOf(Props[TaskHandler])
      val msg = CommandTask(self, TurnToHandleJson.empty)
      echo ! msg
      expectNoMessage(Duration(1000, "millis"))
    }

    // JoinGame
    "send JoinGameReq to engine in response to TurnToHandle with JoinGame msg and with sender's ActorRef " in {
      val echo = system.actorOf(Props[TaskHandler])
      val msg = CommandTask(self, TurnToHandleJson.empty(JoinGame))
      echo ! msg
      expectMsg(EngineService.JoinGameReq(self, 1, "", TurnToHandleJson.empty(JoinGame).currState))
    }

    // ExitRoom
    "send ExitGameReq to engine in response to TurnToHandle with ExitRoom msg and with sender's ActorRef " in {
      val echo = system.actorOf(Props[TaskHandler])
      val msg = CommandTask(self, TurnToHandleJson.empty(ExitRoom))
      echo ! msg
      expectMsg(EngineService.ExitGameReq(self,msg.turnToHandle))
    }

    // EndTurn
    "send Turn to engine in response to TurnToHandle with EndTurn msg and with sender's ActorRef " in {
      val echo = system.actorOf(Props[TaskHandler])
      val msg = CommandTask(self, TurnToHandleJson.empty(EndTurn))
      echo ! msg
      expectMsg(EngineService.Turn(self,msg.turnToHandle))
    }

    // FallBackToTurn в случае удачной расшифровки
    "send FallBackToTurn to engine in response to TurnToHandle" +
      " with FallBackToTurn msg and valid turnNum and with sender's ActorRef " in {
      val echo = system.actorOf(Props[TaskHandler])
      val turnNum = 1
      val msg = CommandTask(self, TurnToHandleJson.empty(FallbackToTurn).copy(extraInfo = turnNum.toString))
      echo ! msg
      expectMsg(EngineService.FallBackToReq(self, turnNum, msg.turnToHandle.currState))
    }

    // FallBackToTurn в случае неудачной расшифровки
    "send Turn to engine in response to TurnToHandle with FallBackToTurn msg" +
      " and invalid turnNum and with sender's ActorRef " in {
      val echo = system.actorOf(Props[TaskHandler])
      val invalidTurnNum = "1a"
      val msg = CommandTask(self, TurnToHandleJson.empty(FallbackToTurn).copy(extraInfo = invalidTurnNum))
      echo ! msg
      expectMsg(EngineService.Turn(self, msg.turnToHandle.copy(command = EndTurn)))
    }

  }
}
