package re.server

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import re.server.core.db.StorageHandler.{DBRetrieveState, DBSave}

import java.util.UUID

class StorageMock(engineRef: ActorRef, uuid: UUID, testProbeRef: ActorRef) extends Actor with ActorLogging {

  override def receive: Receive = {
    case _: DBSave => testProbeRef ! "OK SAVED"

    case _: DBRetrieveState => testProbeRef ! "OK REQUEST FOR STATE"

    case _ =>
  }
}


object StorageMock {
  def props(engineRef: ActorRef, uuid: UUID, testProbeRef: ActorRef): Props =
    Props(new StorageMock(engineRef, uuid, testProbeRef))
}
