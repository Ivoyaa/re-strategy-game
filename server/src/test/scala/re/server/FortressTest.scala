package re.server

import org.scalatest.funsuite.AnyFunSuite
import re.server.game.basicStructures.{InGame, Player, Point, State}
import re.server.game.units.{Fortress, Swordsman}

class FortressTest extends AnyFunSuite {

  def getStateWithFortressFromPoint(p: Point): State =
    State(0, List(Player(0,"",InGame,"",200,Fortress(p,20),List.empty)))

  val fortress: Fortress = Fortress(Point(2,2), 20)
  val fortressOnTheEdge: Fortress = Fortress(Point(1,1), 20)

  // Fortress in (2,2) cell
  val oneFortressState: State = getStateWithFortressFromPoint(Point(2,2))
  val oneFortressOnTheEdge: State = getStateWithFortressFromPoint(Point(1,1))

  // Warfare unit in (1,1) cell, fortress in (2,2)
  val oneWarfareUnitState: State =
    State(0, List(Player(0,"",InGame,"",200,Fortress(Point(2,2),20),List(Swordsman(3, Point(1,1))))))

  val  oneForeignWarfareUnitState: State =
    State(0, List(
      Player(0,"",InGame,"",200,Fortress(Point(2,8),20),List(Swordsman(3, Point(1,1)))) // чужой юнит занял клетку
      ,Player(0,"",InGame,"",200,Fortress(Point(2,2),20),List.empty)
    ))

  val unitsForTestProduction: List[Swordsman] = {for (
    x <- -1 to 1;
    y <- -1 to 1;
    if (x,y) != (0,0)
  ) yield Swordsman(
    Swordsman.defaultHp, Point(fortress.position.x + x, fortress.position.y + y)
  )}.toList

  val fullyOccupiedState: State =
    State(0, List(Player(0,"",InGame,"",200,Fortress(Point(2,2),20),unitsForTestProduction)))

  test("Fortress.spawnUnit") {
    // проверяет, что юнит производится в клетке (1,1) для базы в точке (2,2)
    assert(
      fortress.spawnUnit(Swordsman.unitCode, oneFortressState)
        === Some(Swordsman(Swordsman.defaultHp, Point(1,1)))
    )

    // проверяет, что при наличии другого юнита этого же игрока в клетке (1,1),
    // новый юнит произведется в соседней клетке (1,2)
    assert(
      fortress.spawnUnit(Swordsman.unitCode, oneWarfareUnitState)
      === Some(Swordsman(Swordsman.defaultHp, Point(1,2)))
    )

    // проверяет, что при наличии другого юнита другого игрока в клетке (1,1),
    // новый юнит произведется в соседней клетке (1,2)
    assert(
      fortress.spawnUnit(Swordsman.unitCode, oneForeignWarfareUnitState)
        === Some(Swordsman(Swordsman.defaultHp, Point(1,2)))
    )

    // проверяет факт, что при занятости всех клеток вокруг крепости, юнит произведен не будет
    assert(
      fortress.spawnUnit(Swordsman.unitCode, fullyOccupiedState)
        === None
    )

    // проверяет, что крепость, находящася на границе карты произведет юнит
    // в клетке внутри карты
   assert(
     fortressOnTheEdge
       .spawnUnit(Swordsman.unitCode, oneFortressOnTheEdge)
     === Some(Swordsman(Swordsman.defaultHp, Point(1,2)))
   )

  }
}
