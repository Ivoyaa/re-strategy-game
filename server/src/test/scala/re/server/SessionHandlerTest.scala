package re.server

import akka.actor.ActorSystem
import akka.io.Tcp
import akka.io.Tcp.{Received, Write}
import akka.testkit.{ImplicitSender, TestActors, TestKit, TestProbe}
import akka.util.ByteString
import io.circe.syntax.EncoderOps
import org.scalatest.{BeforeAndAfterAll, _}
import re.server.core.TaskHandler.CommandTask
import re.server.front.SessionHandler
import re.server.front.SessionHandler.{Heartbeat, Send}
import re.server.game.basicStructures.{State, TurnToHandleJson, YourTurn}




class SessionHandlerTest()
  extends TestKit(ActorSystem("server"))
    with ImplicitSender
    with wordspec.AnyWordSpecLike
    with  org.scalatest.matchers.should.Matchers
    with BeforeAndAfterAll {

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A SessionHandler actor" should {

    //проверка на наличие соединения
    "send message ByteString(\"Ping\") as response to Heartbeat object" in {
      val echo = system.actorOf(SessionHandler.props(1, self))
      echo ! Heartbeat
      expectMsg(Write(ByteString("Ping")))
    }

    //отправка изнутри сервера наружу
    "send ByteString of serialized to json-string YourTurn instance in response to Send(YourTurn) message " in {
      val echo = system.actorOf(SessionHandler.props(1, self))
      val msg = YourTurn(0, State.empty, "")
      echo ! Send(msg)
      expectMsg(Write(ByteString(msg.asJson.toString())))
    }

    //отправка снаружи внуть сервера
    "send CommandTask(self, deserialized TurnToHandle) instance in response to serialized TurnToHandle msg " in {
      val echo = system.actorOf(SessionHandler.props(1, self))
      // так как внутрь SessionHandler-а заложен путь куда он должен дальше посылать сообщения для обработки,
      // приходиться имитировать нахождения актора с именем task по этому пути
      val mockActor = system.actorOf(TestActors.forwardActorProps(self), "task")
      val msg = ByteString(TurnToHandleJson.empty.asJson.toString())
      echo ! Received(msg)
      expectMsg(CommandTask(echo, TurnToHandleJson.empty))
    }

    "terminate after receiving message about closing TCP connection" in {
      val target = system.actorOf(SessionHandler.props(1, self))
      val probe = TestProbe()
      probe.watch(target)
      target ! Tcp.Closed
      probe.expectTerminated(target)
    }

  }
}
